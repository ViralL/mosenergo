'use strict';

//scroll to top
if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}
//scroll to top

// ready
$(document).ready(function () {

    //greedymenu
    $(function () {
        var $btn = $('.overflow-trigger');
        var $vlinks = $('.greedy-links');
        var $hlinks = $('.overflow-main');
        var $hlink = $('.overflow-main .overflow');
        var numOfItems = 0;
        var totalSpace = 0;
        var breakWidths = [];
        $vlinks.children().outerWidth(function (i, w) {
            totalSpace += w;
            numOfItems += 1;
            breakWidths.push(totalSpace);
        });
        var availableSpace, numOfVisibleItems, requiredSpace;

        function check() {
            availableSpace = Math.round($vlinks.width() - 10);
            console.log('availableSpace:'+availableSpace);
            numOfVisibleItems = $vlinks.children().length;
            requiredSpace = Math.round(breakWidths[numOfVisibleItems - 1]);
            console.log('requiredSpace:'+requiredSpace);
            if (requiredSpace > availableSpace) {
                $vlinks.children().last().prependTo($hlink);
                numOfVisibleItems -= 1;
                check();
            } else if (availableSpace > breakWidths[numOfVisibleItems]) {
                $hlink.children().first().appendTo($vlinks);
                numOfVisibleItems += 1;
            }
            $btn.attr("count", numOfItems - numOfVisibleItems);
            if (numOfVisibleItems === numOfItems) {
                $btn.addClass('hidden');
            } else $btn.removeClass('hidden');
        }
        function checkUpdate() {
            if(window.outerWidth <= 1365) {
                setTimeout(function () {
                    check();
                }, 500);
            }
        }
        $(window).resize(function () {
            checkUpdate();
        });
        $(window).on("orientationchange", function () {
            checkUpdate();
        });
        checkUpdate();

        setTimeout(function () {
            $('.greedy').css('opacity', '1');
        }, 700);
        $btn.on('click', function () {
            $hlinks.toggleClass('hidden');
            if ($(window).width() < 768) {
                $('body').addClass('fixed');
            }
        });
        $('.overflow-close--js').on('click', function () {
            $hlinks.addClass('hidden');
            $('body').removeClass('fixed');
        });
    });
    // //greedymenu

    //.page-header__search--js
    $('.page-header__search--js').click(function () {
        $('.mainsearch').toggleClass('active');
    });
    $('.mainsearch-close--js').click(function () {
        $('.mainsearch').removeClass('active');
    });
    $('.showhide-txt--js').click(function () {
        $(this).toggleClass('active')
        $('.white-popup-inner').toggleClass('hideme');
    });
    //.page-header__search--js


    //simplepopup
    $('.addpopup').click(function () {
        $('#letter-id').addClass('active');
        $('.popup-background').addClass('active');
        $('body').addClass('fixed');
        return false;
    });
    $('.popup-background, .popup-close--js').click(function () {
        $('#letter-id').removeClass('active');
        $('.popup-background').removeClass('active');
        $('body').removeClass('fixed');
        return false;
    });
    //simplepopup

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999)999-99-99");
    // mask phone


    // slider {http://idangero.us/swiper/}
    var swiper = new Swiper('.swiper', {
        slidesPerView: 'auto',
        spaceBetween: 80,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            767: {
                // slidesPerView: 1,
                spaceBetween: 20
            }
        }
    });
    var swiperFull = new Swiper('.swiper-full', {
        slidesPerView: 2,
        loop: true,
        pagination: {
            el: '.swiper-paginationf',
            clickable: true
        }
    });
    var swiperFullMob = new Swiper('.swiper-fullmob', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-paginationfl',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-buttonfl-next',
            prevEl: '.swiper-buttonfl-prev',
        }
    });
    var swiperSl = new Swiper('.swiper-sl', {
        slidesPerView: 3,
        // spaceBetween: 2,
        // loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            1700: {
                slidesPerView: 2
            },
            767: {
                slidesPerView: 1
            }
        }
    });
    var swiperNw = new Swiper('.swiper-nw', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    var swiperFl = new Swiper('.swiper-fl', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-paginationfl',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-buttonfl-next',
            prevEl: '.swiper-buttonfl-prev',
        }
    });
    var swiperDir = new Swiper('.swiper-dir', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 20,
        pagination: {
            el: '.swiper-paginationdir',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-buttondir-next',
            prevEl: '.swiper-buttondir-prev',
        }
    });
    $('.tumb .swiper-slide').click(function () {
       var slideNumber = $(this).data('slide');
        galleryTop.slideTo(parseInt(slideNumber-1), 1000, true);
    });
    // slider

    //fullpage
    $('#fullpage').fullpage({
        anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', '5thpage'],
        navigation: true,
        // autoScrolling: false,
        navigationPosition: 'left',
        responsiveWidth: 1025,
        navigationTooltips: ['Главная', 'История', 'Направления деятельности', 'Новости', 'Партнеры'],
        afterRender: function () {
            $('#fp-nav').append('<span class="arrow arrowUp"></span><span class="arrow arrowDown"></span>');
            setTimeout(function () {
                swiperNw.update();
                swiperFl.update();
                swiperDir.update();
                swiperFull.update();
                swiperFullMob.update();
            }, 100);
        }
    });
    $('.arrowUp').click(function () {
        $.fn.fullpage.moveSectionUp();
    });
    $('.arrowDown').click(function () {
        $.fn.fullpage.reBuild();
    });
    //fullpage

    function swiperUpdates() {
        setTimeout(function () {
            if($('.swiper-nw').length) {
                swiperNw.update();
            }
            if($('.swiper-fl').length) {
                swiperFl.update();
            }
            if($('.swiper-dir').length) {
                swiperDir.update();
            }
            if($('.swiper-full').length) {
                swiperFull.update();
            }
            if($('.swiper-fullmob').length) {
                swiperFullMob.update();
            }
        }, 500);
    }

    $(window).resize(function () {
        swiperUpdates();
    });
    $(window).on("orientationchange", function () {
        swiperUpdates();
    });

});